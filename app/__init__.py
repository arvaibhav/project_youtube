
from celery import Celery
from elasticsearch import Elasticsearch
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_bootstrap import Bootstrap
from app.config import Config
from flask_moment import Moment
from app.users.errors import page_not_found
db = SQLAlchemy()
migrate = Migrate()
login = LoginManager()
login.login_view = 'users.view'
bootstrap = Bootstrap()
moment = Moment()
celery = Celery('app', broker='redis://localhost:6379', backend="redis://localhost:6379")
celery.conf.update(CELERY_IMPORTS='app.users.routes')
app = Flask(__name__)


def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)

    db.init_app(app)
    migrate.init_app(app, db)
    login.init_app(app)

    bootstrap.init_app(app)
    moment.init_app(app)

    from app.users.routes import user
    from app.videos.routes import video

    app.register_blueprint(user)
    app.register_blueprint(video)
    app.register_error_handler(404, page_not_found)

    app.elasticsearch = Elasticsearch([app.config['ELASTICSEARCH_URL']]) if app.config[
        'ELASTICSEARCH_URL'] else Elasticsearch('http://localhost:9200')

    # celery.__init__(app.name, broker=app.config['CELERY_BROKER_URL'],backend=app.config['CELERY_RESULT_BACKEND'])

    celery.conf.update(app.config)

    return app
