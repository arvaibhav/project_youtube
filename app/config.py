import os
import logging

logging.basicConfig(filename='app.log', filemode='w', format='%(asctime)s %(name)s - %(levelname)s - %(message)s')

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    # ...

    SQLALCHEMY_DATABASE_URI =os.environ.get('DATABASE_URL')
    if not SQLALCHEMY_DATABASE_URI:
        logging.critical('SQLALCHEMY_DATABASE_URI not set')

    SQLALCHEMY_TRACK_MODIFICATIONS = False

    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'

    POSTS_PER_PAGE = os.environ.get(('POSTS_PER_PAGE')) or 20

    ELASTICSEARCH_URL = os.environ.get('ELASTICSEARCH_URL')

    if not ELASTICSEARCH_URL:
        logging.critical('ELASTICSEARCH_URL not set')
    CELERY_BROKER_URL = os.environ.get('CELERY_BROKER_URL')

    if not CELERY_BROKER_URL:
        logging.critical('CELERY_BROKER_URL not set')

    CELERY_RESULT_BACKEND = os.environ.get('CELERY_RESULT_BACKEND')
    if not CELERY_RESULT_BACKEND:
        logging.critical('CELERY_RESULT_BACKEND not set')

