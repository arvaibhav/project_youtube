from datetime import datetime

from sqlalchemy.orm import session

from app import db
from flask_login import UserMixin
from app import login
from werkzeug.security import generate_password_hash, check_password_hash
from hashlib import md5
from app.utils import get_thumb_from_video
import os
from app.search import add_to_index, query_index, remove_from_index
from app.search import add_to_index, remove_from_index, query_index


class SearchableMixin(object):
    @classmethod
    def search(cls, expression, page, per_page):
        ids, total = query_index(cls.__tablename__, expression, page, per_page)
        if total == 0:
            return cls.query.filter_by(video_id=0), 0
        when = []
        for i in range(len(ids)):
            when.append((ids[i], i))
        return cls.query.filter(cls.video_id.in_(ids)).order_by(
            db.case(when, value=cls.video_id)), total

    @classmethod
    def before_commit(cls, session):
        session._changes = {
            'add': list(session.new),
            'update': list(session.dirty),
            'delete': list(session.deleted)
        }

    @classmethod
    def after_commit(cls, session):
        for obj in session._changes['add']:
            if isinstance(obj, SearchableMixin):
                add_to_index(obj.__tablename__, obj)
        for obj in session._changes['update']:
            if isinstance(obj, SearchableMixin):
                add_to_index(obj.__tablename__, obj)
        for obj in session._changes['delete']:
            if isinstance(obj, SearchableMixin):
                remove_from_index(obj.__tablename__, obj)
        session._changes = None

    @classmethod
    def reindex(cls):
        for obj in cls.query:
            add_to_index(cls.__tablename__, obj)


db.event.listen(db.session, 'before_commit', SearchableMixin.before_commit)
db.event.listen(db.session, 'after_commit', SearchableMixin.after_commit)


@login.user_loader
def load_user(id):
    return User.query.get(int(id))


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))

    videos = db.relationship('Video', backref='author', lazy='dynamic')

    about_me = db.Column(db.String(140))

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def avatar(self, size):
        digest = md5(self.email.lower().encode('utf-8')).hexdigest()
        return 'https://www.gravatar.com/avatar/{}?d=identicon&s={}'.format(
            digest, size)

    profile_pic_path = db.Column(db.String(340))

    def set_initial_profile_pic(self):
        digest = md5(self.email.lower().encode('utf-8')).hexdigest()
        self.profile_pic_path = 'https://www.gravatar.com/avatar/{}?d=identicon&s={}'.format(
            digest, 220)

    def __repr__(self):
        return '<User {}>'.format(self.username)


class Video(SearchableMixin, UserMixin, db.Model):
    # title need to be indexed
    __tablename__ = 'video'
    __searchable__ = ['title', 'description']
    video_id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    title = db.Column(db.String(180), index=True, unique=True)
    description = db.Column(db.String(480))
    video_image_thumb = db.Column(db.String(280))
    time_of_upload = db.Column(db.DateTime, default=datetime.utcnow)
    duration = db.Column(db.Integer)
    like = db.Column(db.Integer, default=0)
    dislike = db.Column(db.Integer, default=0)

    comments = db.relationship('Comment', backref='video_comments', lazy='dynamic')

    number_of_views = db.Column(db.Integer, default=0)
    last_view_date = db.Column(db.DateTime, default=datetime.utcnow())
    video_path = db.Column(db.String(480))
    video_changes = db.relationship('VideoChange', backref='video_change', lazy='dynamic')

    def set_video_meta_detail(self):
        video_meta_detail = get_thumb_from_video(os.getcwd() + '/app' + self.video_path, self.title)

        self.video_image_thumb = video_meta_detail['thumb_image_path']
        self.duration = video_meta_detail['duration']


class Comment(UserMixin, db.Model):
    comment_id = db.Column(db.Integer, primary_key=True)
    video_id = db.Column(db.Integer, db.ForeignKey('video.video_id'))
    # user ID
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    comment_by = db.Column(db.String(480))
    comment = db.Column(db.String(480))
    like = db.Column(db.Integer, default=0)
    dislike = db.Column(db.Integer, default=0)
    comments_changes = db.relationship('CommentChange', backref='video_comments_change', lazy='dynamic')


class VideoChange(UserMixin, db.Model):
    video_change_id = db.Column(db.Integer, primary_key=True, index=True)

    video_id = db.Column(db.Integer, db.ForeignKey('video.video_id'))

    video_change_by_user_id = db.Column(db.Integer, db.ForeignKey('user.id'), index=True)

    video_like_change = db.Column(db.Boolean, default=False)
    video_dislike_change = db.Column(db.Boolean, default=False)
    # video_change_by_unregistered_user_id=db.Column(db.String(180),index=True)


class CommentChange(UserMixin, db.Model):
    comment_change_id = db.Column(db.Integer, primary_key=True, index=True)
    comment_id = db.Column(db.Integer, db.ForeignKey('comment.comment_id'))
    comment_change_by_user_id = db.Column(db.Integer, db.ForeignKey('user.id'), index=True)
    comment_like_change = db.Column(db.Boolean, default=False)
    comment_dislike_change = db.Column(db.Boolean, default=False)
