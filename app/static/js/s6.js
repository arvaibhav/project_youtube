var comments;
$(document).ready(function(e) {


  $.ajax({

    type: 'GET',
    url: '/get-video-data/' + videoID

  }).done(function(datas) {
    comments = datas.comments;
    loadComments()

  });



  $(".video-comments-form input").click(function(e) {
    e.preventDefault();
  });
  $(".video-comments-form input[type=submit]").click(function() {
    let a = $(".video-comments-form textarea").val();

    if (a !== '') {

      $.ajax({

        type: 'GET',
        data: {
          comment: a
        },
        url: '/add-comment/' + videoID
      }).done(function(datas) {
        if (!datas.error) {
          comments = datas.comments;

          loadComments()
          alert('Added Comment')
          $(".video-comments-form textarea").val("");
        } else {
          alert('Please Login ..')
        }
      });

    }


  });



  $(".video-likes button").click(function(e) {
    e.preventDefault();
    var button=$(this)
      var total_likes=parseInt($(this).attr('likes'));
      $.ajax({
      url:'/like_video/'+videoID,
      type:'GET',
      data:{
      total_likes:total_likes
      }
      }).done(function(datas){
      if('error' in datas)
      {
      alert(datas.error)
      }
      else
      {
      if(datas.status==true)
      {
      button.text((total_likes+1)+' likes')
      }
      }
      });

  });

  $(".video-dislikes button").click(function(e) {
    e.preventDefault();
     var total_dislikes=$(this).attr('dislikes') ;

        $.ajax({
      url:'/dislike_video/'+videoID,
      type:'GET',
      data:{
      total_likes:total_dislikes
      }
      }).done(function(datas){
        console.log(datas)
      });

  });


$(".video-dislikes button").click(function(e) {
    e.preventDefault();
    var button=$(this)
      var total_likes=parseInt($(this).attr('dislikes'));
      $.ajax({
      url:'/dislike_video/'+videoID,
      type:'GET',
      data:{
      total_likes:total_likes
      }
      }).done(function(datas){
      if('error' in datas)
      {
      alert(datas.error)
      }
      else
      {
      if(datas.status==true)
      {
      button.text((total_likes+1)+' DisLikes')
      }
      }
      });

  });


});


function loadComments() {

  $('.comments-list-container').empty()

  for (let i = 0; i < comments.length; i++) {

    let comment = comments[i][0];
    let like = comments[i][1];
    let dislike = comments[i][2];
    let comment_id = comments[i][3];

    let username = comments[i][4];
    let user_id = comments[i][5];
    let user_image = comments[i][6];

    setComment(user_id, username, user_image, comment, like, dislike, comment_id)

  }
}

function setComment(user_id, username, user_image, comment, like, dislike, comment_id) {

  let commentsListItem = $('<div/>', {
    'class': 'comments-list-item',

  });

  let commentUserProfileImage = $('<div/>', {
    'class': 'comment-user-profile-image'
  }).append($('<a/>', {
    'href': "#"
  })).append($('<img/>', {
    src: user_image
  }));
  // ______________________________comment-detail_________
  let commentDetail = $('<div/>', {
    'class': 'comment-detail'
  })

  let commentUserName = $('<div/>', {
    'class': 'commment-user-name'
  }).append($('<a/>', {
    'href': '',
    text: username
  }))

  let commentDescription = $('<div/>', {
    'class': 'commment-description',
    text: comment
  })

  let commentLikesDislikes = $('<div/>', {
    'class': 'commentlikes-dislikes'
  })

  var commentLikes = $('<div/>', {
    'class': 'comment-like-dislike-head',
    text: 'Likes :'
  }).append($('<button/>', {

    'class': 'like-dislike-button',
    text: like
  })).click(function() {
    //  commentLikes.children().text(parseInt("5"));
    $.ajax({

      type: 'GET',
      data: {
        comment_id: comment_id,
        islike: 'True'
      },
      url: '/like-dislike-comment/' + comment_id
    }).done(function(datas) {

      if (!datas.user_logined)
        alert('Please Login')
      else {
        if (datas.tolike) {
          console.log(parseInt(commentLikes.children().text()))
          var val = parseInt(commentLikes.children().text());

          commentLikes.children().text(parseInt(val + 1));
        }
      }

    });
    //    $(this).remove();
  })

  let commentDislikes = $('<div/>', {
    'class': 'comment-like-dislike-head',
    text: 'DisLikes :'
  }).append($('<button/>', {

    'class': 'like-dislike-button',
    text: dislike
  })).click(function() {
    $.ajax({

      type: 'GET',
      data: {
        comment_id: comment_id,
        islike: 'False'
      },
      url: '/like-dislike-comment/' + comment_id
    }).done(function(datas) {

      if (!datas.user_logined)
        alert('Please Login')
      else {

        if (datas.tolike) {
          console.log(parseInt(commentDislikes.children().text()))
          var val = parseInt(commentDislikes.children().text());

          commentDislikes.children().text(parseInt(val + 1));
        }
      }

    });
  })


  commentDetail.append(commentUserName);
  commentDetail.append(commentDescription);
  commentDetail.append(commentLikesDislikes);

  commentLikesDislikes.append(commentLikes);
  commentLikesDislikes.append(commentDislikes);


  commentsListItem.append(commentUserProfileImage);
  commentsListItem.append(commentDetail);
  commentsListItem.appendTo('.comments-list-container');

}