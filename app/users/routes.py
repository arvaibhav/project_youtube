import os
from pathlib import Path

from werkzeug.utils import redirect, secure_filename
from app.models import User, Video
from flask import render_template, url_for, flash, request, current_app
from app import db
from app.users.forms import LoginForm, RegistrationForm, UploadVideoForm
from flask import Blueprint
from flask_login import current_user, login_user, login_required, logout_user
from app import celery

user = Blueprint('users', __name__)
from flask import jsonify
from app import create_app


@user.route('/dashboard/<username>', methods=['GET', 'POST'])
@login_required
def user_page(username):
    upload_form = UploadVideoForm()
    if upload_form.validate_on_submit():
        video_file_from_form = upload_form.videofile.data
        filename = secure_filename(video_file_from_form.filename)
        local_video_file_path = os.path.join(os.getcwd(), 'app', 'static', 'database', 'video', filename)
        video_file_path = os.path.join('/static', 'database', 'video', filename)
        try:
            os.makedirs(Path(os.path.abspath(local_video_file_path)).parent)
        except:
            pass
        video_file_from_form.save(os.path.abspath(local_video_file_path))
        video = Video(title=upload_form.title.data, description=upload_form.description.data,
                      video_path=video_file_path, user_id=current_user.id)
        video.set_video_meta_detail()
        db.session.add\
            (video)
        db.session.commit()
        flash('uploaded video')
        return redirect(url_for('users.user_page', username=current_user.username))
    return render_template('user.html', user=current_user, total_video_count=current_user.videos.count(),
                           upload_form=upload_form
                           , videos=current_user.videos)


@user.route('/qetdata', methods=['GET', 'POST'])
def request_data():
    import time

    user_id = current_user.id
    result = generate_data.delay(user_id)
    time.sleep(5)
    result_output = result.info
    result.wait(20)
    print(result.get())

    return jsonify(result.info)


@celery.task
def generate_data(user_id):
    import os
    basedir = os.path.abspath('./app')

    app = create_app()

    app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('DATABASE_URL') or 'postgresql+psycopg2://skyvaib:7747@localhost/appdb'
    with app.app_context():
        id = int(user_id)
        user = User.query.filter_by(id=id).first()

        return {'videos data': [(x.video_id, x.title, x.description, x.time_of_upload, x.like, x.dislike) for x in
                                user.videos]}


@user.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('videos.index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return render_template('login.html', title='Sign In', form=form)
        login_user(user, remember=form.remember_me.data)
        # print(user.username)
        return redirect(url_for('videos.index'))
    return render_template('login.html', title='Sign In', form=form)



@user.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('videos.index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        user.set_initial_profile_pic()
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('users.login'))
    return render_template('register.html', form=form)


@user.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('videos.index'))


@user.route('/edit')
def edit_profile():
    new_username=request.args['user_username']
    new_email=request.args['user_email']
    chk_user=User.query.filter_by(username=new_username)
    if current_user.username != new_username:
        if chk_user:
            return jsonify({'result': 'User name already there please give any other'})


    current_user.username=new_username
    current_user.email=new_email
    db.session.commit()
    return jsonify({'result': 'changed'})