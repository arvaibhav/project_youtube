from app import celery
import cv2
import os
import math
import re

from flask import current_app


def get_thumb_from_video(video_path, thumb_name):
    print(video_path)

    thumb_name = re.sub('[@#$%^&*() ]', '_', thumb_name)

    # Read the video from specified path
    cam = cv2.VideoCapture(video_path)
    thumb_path = str()
    try:
        # creating a folder named data
        # creating a folder named data
        thumb_path = os.path.join(os.getcwd(), 'app', 'static', 'database', 'videos_thumbs')

        if not os.path.exists(thumb_path):
            os.makedirs(thumb_path)
            print(thumb_path)
        # if not created then raise error
    except OSError:
        print('Error: Creating directory of data')

    cam.read()
    ret, frame = cam.read()
    if ret:
        thumb_image_path = os.path.abspath(thumb_path + '/' + thumb_name + '.jpg')

        cv2.resize(frame, (120, 120))
        cv2.imwrite(thumb_image_path, frame)

        fps = cam.get(cv2.CAP_PROP_FPS)  # OpenCV2 version 2 used "CV_CAP_PROP_FPS"

        frame_count = int(cam.get(cv2.CAP_PROP_FRAME_COUNT))
        duration = frame_count / fps

        # minutes = int(duration / 60)
        # seconds = duration % 60
        # duration = (str(minutes) + ':' + str(math.ceil(seconds)))
        cam.release()
        cv2.destroyAllWindows()
        local_thumb_path = '/' + os.path.join('static', 'database', 'videos_thumbs', (thumb_name + '.jpg'))

        return {'thumb_image_path': local_thumb_path, 'duration': duration}
