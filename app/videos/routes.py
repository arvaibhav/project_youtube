from datetime import datetime

from werkzeug.utils import redirect
from app.models import User, Video, Comment, VideoChange, CommentChange
from flask import render_template, url_for, flash, request, current_app, make_response, session, g, jsonify
from flask import Blueprint
from app import db
from sqlalchemy import desc
from flask_login import current_user, login_required
from app.videos.forms import CommentForm, SearchForm
video = Blueprint('videos', __name__)


def do_comment_like(to_like, comment_id):
    if not current_user.is_authenticated:
        return False

    comment = Comment.query.filter_by(comment_id=comment_id).first()
    comment_change = comment.comments_changes.filter_by(comment_change_by_user_id=int(current_user.id)).first()

    if not comment_change:
        comment_change = CommentChange(comment_change_by_user_id=int(current_user.id), comment_id=comment_id)
        db.session.add(comment_change)
    if to_like:
        if not comment_change.comment_like_change:
            comment_change.comment_like_change = True
            comment.like = comment.like + 1
            db.session.commit()
            return True
        else:
            return False
    else:
        if not comment_change.comment_dislike_change:
            comment_change.comment_dislike_change = True
            comment.dislike = comment.dislike + 1
            db.session.commit()
            return True
        else:
            return False


def do_video_like(to_like, video):


    video_id = str(video.video_id)
    if 'video_change' not in session:
        create_cookie_for_video_change(video_id, current_user.id, video)
    else:

            if video_id in session['video_change']:
                if str(current_user.id) not in session['video_change'][video_id]:
                    session['video_change'][video_id][str(current_user.id)].update({'dislike': False, 'like': False})
                if to_like:

                    if session['video_change'][video_id][str(current_user.id)]['like'] is False:

                        video.like = video.like + 1
                        # check and update in db
                        videochange = video.video_changes.filter_by(video_id=int(video_id),
                                                                    video_change_by_user_id=current_user.id).first()
                        if videochange:
                            videochange.video_like_change = True;
                            db.session.commit()
                        else:
                            videochange = VideoChange(video_id=int(video_id), video_change_by_user_id=current_user.id,
                                                      video_like_change=True)
                            db.session.add(videochange)
                            db.session.commit()

                        session['video_change'][video_id][str(current_user.id)] = {
                            'dislike': session['video_change'][video_id][str(current_user.id)]['dislike'], 'like': True}




                else:
                    if session['video_change'][video_id][str(current_user.id)]['dislike'] is False:
                        video.dislike = video.dislike + 1

                        session['video_change'][video_id][str(current_user.id)]['dislike'] = 'True'
                        # check and update in db
                        videochange = video.video_changes.filter_by(video_id=int(video_id),
                                                                    video_change_by_user_id=current_user.id).first()
                        if videochange:
                            videochange.video_dislike_change = True;
                            db.session.commit()
                        else:
                            videochange = VideoChange(video_id=int(video_id), video_change_by_user_id=current_user.id,
                                                      video_dislike_change=True)
                            db.session.add(videochange)
                            db.session.commit()

                        session['video_change'][video_id][str(current_user.id)] = {'dislike': True, 'like':
                            session['video_change'][video_id][str(current_user.id)]['like']}

                        return True

                db.session.commit()


            else:

                create_cookie_for_video_change(video_id, current_user.id, video)
    return False


@video.route('/', methods=['GET', 'POST'])
def index():
    videos = Video.query.order_by(desc(Video.number_of_views)).all()
    return render_template('index.html', videos=videos, trendings=videos[0:4])

@video.route('/like_video/<video_id>')
def like_video(video_id):
    if not current_user.is_authenticated:
        return jsonify({'error':'Please Login'})
    video = Video.query.filter_by(video_id=int(video_id)).first()

    if video:
        status=do_video_like(True, video)

        session['video_change'] = session['video_change'].copy()

        return jsonify({'status':status})
    else:
        return jsonify({'error':'video not found'})


@video.route('/dislike_video/<video_id>')
def dislike_video(video_id):
    if not current_user.is_authenticated:
        return jsonify({'error': 'Please Login'})
    video = Video.query.filter_by(video_id=int(video_id)).first()

    if video:
        status = do_video_like(False, video)

        session['video_change'] = session['video_change'].copy()

        return jsonify({'c': status})
    else:
        return jsonify({'error': 'video not found'})



@video.route('/view_video/<video_id>', methods=['GET', 'POST'])
def view_video(video_id):
    video = Video.query.filter_by(video_id=int(video_id)).first()


    commentform = CommentForm()

    # print(request.remote_addr)

    if video:
        # check value in session
        if 'video_change' in session:
            # print(session['video_change'])
            if video_id in session['video_change']:

                if current_user.is_authenticated:

                    if str(current_user.id) in session['video_change'][video_id]:

                        pass
                    else:

                        create_cookie_for_video_change(video_id,
                                                       current_user.id,
                                                       video)
                else:
                    if 'unregistered_user' in session['video_change'][video_id]:
                        pass

                    else:
                        create_cookie_for_video_change(video_id,
                                                       'unregistered_user',
                                                       video)

                # if video is already watched then skip to increase view

            else:
                create_cookie_for_video_change(video_id,
                                               current_user.id if current_user.is_authenticated else 'unregistered_user',
                                               video)

        else:
            create_cookie_for_video_change(video_id,
                                           current_user.id if current_user.is_authenticated else 'unregistered_user',
                                           video)

        #

        user_comments = User.query.join(Comment).add_columns(Comment.comment,
                                                             Comment.like, Comment.dislike,
                                                             Comment.comment_id).filter(
            User.id == Comment.user_id). \
            filter(Comment.video_id == video_id).all()

        video_uploader = User.query.filter_by(id=video.user_id).first_or_404()

        return render_template('view_video.html', video=video, user_comments=user_comments, commentform=commentform,
                               user=video_uploader)



    else:
        return 'video deleted'


def create_cookie_for_video_change(video_id, client_id, video):

    # import pdb
    # pdb.set_trace()
    add_cookie = {video_id: {client_id: {'dislike': False, 'like': False}}}
    if current_user.is_authenticated:
        video_change = VideoChange.query.filter_by(video_id=video_id, video_change_by_user_id=int(client_id)).first()

        if not video_change:
            videochange = VideoChange(video_id=video_id, video_change_by_user_id=client_id)
            db.session.add(videochange)

            video.number_of_views = video.number_of_views + 1
            video.last_view_date = datetime.utcnow()
            db.session.commit()
        else:
            add_cookie = {video_id: {
                client_id: {'dislike': video_change.video_dislike_change, 'like': video_change.video_like_change}}}
            pass
    # for new unregistered user
    else:
        video.number_of_views = video.number_of_views + 1
        video.last_view_date = datetime.utcnow()
        db.session.commit()

    if 'video_change' in session:
        temp_dict = dict(session['video_change'])
        temp_dict.update(add_cookie)
        session['video_change'] = temp_dict

    else:

        session['video_change'] = {video_id: {client_id: {'dislike': False, 'like': False}}}
    print('chanded', session['video_change'])
        # add video_id {user_id{like,dislike}} in db


@video.before_request
def initialize():
    g.search_form = SearchForm()


@video.route('/search')
def search():
    if not g.search_form.validate():
        return redirect(url_for('videos.index'))

    page = request.args.get('page', 1, type=int)
    videos, total = Video.search(g.search_form.q.data, page,
                                 current_app.config['POSTS_PER_PAGE'])

    next_url = url_for('videos.search', q=g.search_form.q.data, page=page + 1) \
        if total > page * current_app.config['POSTS_PER_PAGE'] else None
    prev_url = url_for('videos.search', q=g.search_form.q.data, page=page - 1) \
        if page > 1 else None
    return render_template('index.html', title=('Search'), videos=videos, trendings=videos[0:4]
                           , next_url=next_url, prev_url=prev_url, total=total)


def create_comment_json(video_id):
    comments = [(x[1], x[2], x[3], x[4], x[5], x[6], x[7]) for x in
                User.query.join(Comment).add_columns(Comment.comment,
                                                     Comment.like, Comment.dislike,
                                                     Comment.comment_id, User.username, User.id,
                                                     User.profile_pic_path).filter(
                    User.id == Comment.user_id). \
                    filter(Comment.video_id == video_id).all()]
    # print(json.dumps(comments))
    videos = [(x.video_id, x.user_id, x.title, x.description, x.duration, x.like, x.dislike) for x in
              Video.query.filter_by(video_id=video_id).all()]

    return jsonify({'videos': videos, 'comments': comments, 'error': False})


@video.route('/add-comment/<video_id>')
def add_comment(video_id):
    comment = request.args.get('comment')

    if current_user.is_authenticated:
        new_comment = Comment(comment=comment, video_id=video_id, comment_by=current_user.username,
                              user_id=current_user.id)
        import pdb;pdb.set_trace();
        db.session.add(new_comment)
        db.session.commit()
    else:
        return jsonify({'error': True})

    return create_comment_json(video_id)


@video.route('/get-video-data/<video_id>')
def get_video_data(video_id):
    return create_comment_json(video_id)


@video.route('/like-dislike-comment/<comment_id>')
def like_dislike_comment(comment_id):
    status = do_comment_like((request.args.get('islike') == 'True'), comment_id)
    return jsonify({'tolike': status, "user_logined": current_user.is_authenticated})

