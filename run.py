import  logging

from app import create_app

app = create_app()

logging.info('App started')
if __name__ == '__main__':
    app.run(debug=True ,port=5000)
